# Mksite

I like how mksite use built in commands, here is [a mirror](https://github.com/clehner/mksite) , but it was lacking in my opinion and I loved how fast and how plug in rich [hugo](https://gohugo.io) is so I tried mixing the two a little.

It has more dependencies than mksite, bashblog, rigid, etc. but it a lot faster than all of them. 

### Dependencies:

- sed (core utilities)

- [markdown.bash](https://github.com/chadbraunduin/markdown.bash)

- imagemagick 

- python

- bash (it might work in other shells but the target shell is bash)

### You are going to want to try this out if:

- You want a shell based static site generator that is fast 

- You like sed (sed is used to find and replace small things in templates and pages; it even minifies HTML output)

- You like markdown and sometimes html

- You are ok with a little experimenting (this is still a WIP and you might need to expiriment to make your own plug ins)and HTML but lazy enough to not want to use HTML all that often


### plug ins:

It has a basic assortment of plugs in right now with some more I plan to add

- Site name

- Page name

- URL of site

- Git Commit Author (for blogging scenario)

- Date (in UTC time)

- image to base64 (for those times when you need to have a small image in your template)

but I plan to implement a comment section as per this intersting [blog post](https://hannes.hauswedell.net/post/2017/10/21/blog-comments/). I like this especially because it provides the choice of using either github or gitea as a commenting provider for hugo. By the time I release this, I will be looking into how to accomplish this exactly.


