# Mksite Configuration

# Name of the site
SITE_NAME="Mksite"

# Development URL
SITE_URL="http://127.0.0.1:8080"

# Production URL
#SITE_URL="https://example.com"

# Leave commented if multiple users make posts
#AUTHOR="John Doe"

# Size of Gravatar avatar
GRAVATAR_SIZE="90"

# The default file ending to convert into webpages
TEXT_FILE="md"

# Add http freindly url if you use SSH
GIT_SOURCE="https://gitlab.com/joedoe47/mksitev2"

