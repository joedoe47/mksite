## why

> **![joedoe47 Gravatar](https://s.gravatar.com/avatar/57d31237a16d306df30f99ee53220d2b?s=90)**

> Author: joedoe47

> Date: Fri 16 Nov 2018 02:06:19 PM UTC

> Tags: Example, Documentation, Test

> ----------

You may be asking yourself why would I use this generic stite generator as supposed to word press?

- Static sites can be really fast

- Static sites are unhackacle compared to wordpress

- Static sites can be managed using only a text editor, a github/gitlab account, and time

	- Your system administrator can set up a scheduled task to automatically check for updates and automatically build new changes

- Static sites can still have interactive features (comments, mobile/fluid layouts, and even server side scripting; with some work)


Or perhaps you may be asking why should I use mksite v2?

- It mostly written in bash (although it does require a C binary, git, and python)

- Its easy to work with if you are a Linux/Unix system administrator (plug ins are basically sed scripts)

- Its very scriptable and hackable (it can be and was used to generate a bunch of static pages for all of my git repositories)

- Its great for collaboratve writing (because it uses git to determine a few things such as who is the author of a certain post)

- It comes with a lot of plug ins you'd expect to see in hugo (such as minfying html, intellegent compiling, and I plan to add more convieniant features as I use this more (or as people ask for)



