## blog

> **![joedoe47 Gravatar](https://s.gravatar.com/avatar/57d31237a16d306df30f99ee53220d2b?s=90)**

> Author: joedoe47

> Date: Fri 16 Nov 2018 02:06:15 PM UTC

> Tags: Example, Documentation, Test

> ----------


mksite v2 can be used to make whatever your creativeness can conjure. 


**Multi User Blog**

One intersting scenario is using mksitev2 as a multi user blog, similar to how wordpress or medium work.

Thanks to the fact that this script gets information from git and git inherently is meant for collaboration.

you and alice can both be working on new posts at the same time and the script will automatically add the name of the user, their gravatar, and the time they created the post.

Then once Alice and you save (commit) your changes and push them to gitlab or github, changes are automatically tracked. 

If we really wanted to go all out on security, we could go as far as using git's PGP signing capabilities to show your signatues to verify that Alice did indeed work on an article about kerbernetes, while you did in fact push a change for recommended hardware for rapid cloud deployment.

The best part is, other users don't even need to use the mksitev2. Because standard markdown is used, other users can contribute straight from gitlab's web interface. (they have an IDE that allows for markdown) 

Only 1 user actually needs to have mksitev2 or run it and that is the server administrator. (and because it is also a script it can be automated via a webhook, cronjob, or other automation tool)


**Static Git Repository Browser**

One of the basic ideas I had to use mksite v2 for was to have a faster and leaner version of 'git-arr' but more customizable than 'stagit'. Both of these programs are great but they lack a few things such as:

- customizable templates (not just CSS)

- showing PGP signatures of commits

- rendering markdown 

- the ability to render markdown

So mksite v2 can absolutely compete with stagit and git-arr. This is the main reason why I defaulted to using cmark. This script can generate thousands of markdwown files in a cinche. (granted using a script to go through say like 100+ git repositories and getting commits, READMEs, PGP signatures, etc; does take some time even with stagit or git-arr)

**Image Gallary/Portfolio**

It should be obvious but still noted that these sorts of static sites are **awesome** for photo galleries and portfolios that show case how amazing your work is.


