## first_post

> **![joedoe47 Gravatar](https://s.gravatar.com/avatar/57d31237a16d306df30f99ee53220d2b?s=90)**

> Author: joedoe47

> Date: Fri 16 Nov 2018 02:06:26 PM UTC

> Tags: 

> ----------


#### About

This is your first post in mksite v2. By default it supports only markdown due to cmark but in return we get a very fast build times for larger websites.

However it can easily be adapted to use pandoc, which is a little slow, but you can use all sorts of mark up languages. LaTeX, txt, ReSt, docx, etc.

N.B. this uses regular markdown not github style markdown. So you might need to use html and css magic to create tables or make certain things happen.

This software is free and open source available on [github](), [gitlab](), and [self hosted](https://git.joepcs.com/joedoe47/mksitev2). 

Bug fixes, merge requests, etc welcomed.


#### Plugins

This currently is a work in progress. This tool uses sed to act as a method to implement plug ins. It currently will automatically:

- minify webpages

- replace text {{PAGE }} with the name of the page or post

- replace text {{GIT_SOURCE }} with the url of the git remote established on your website to allow others to fork your documentation or blog

- replace text {{SITE_URL }} with the root url of your site

- replace text {{SITE_NAME }} with the title of your site and organization

- replace text {{GRAVATAR }} with the url to your gravatar url to show users your profile image

- create pages/posts for the user with a preset format (and adds new posts, specifically, to an index)

I plan to add a few more practical plug ins. 

Eg. being able to automatically make image sprites or base64 images to improve performance.





